# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How to install ###
*  Download the release
*  In Android Studio use File->Import Module

![import 2.png](https://bitbucket.org/repo/jbdnGr/images/2735730953-import%202.png)

* Add dependency in your build.grade file:
```gradle
dependencies {
    //...
    compile project(':downloadman')
}

```


### How to use ###

```java
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Context context = this;

        final AsyncCallback<DownloadRequest> someCallbackImplementation = new AsyncCallback<DownloadRequest>() {
            @Override
            public void onSuccess(Response<DownloadRequest> response) {
               //your code here
            }

            @Override
            public void onError(Response<DownloadRequest> errorResponse) {
               //your code here
            }

            @Override
            public void onProgressUpdate(ProgressState progressState) {
               //your code here
            }

            @Override
            public void onPreExecute() {
               //your code here
            }
        };

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<DownloadRequest> requests = new ArrayList<DownloadRequest>();
                List<String> titles = new ArrayList<String>();
                List<String> description = new ArrayList<String>();
                List<DownloadTask> tasks = new ArrayList<DownloadTask>();
                IOUtil ioUtil = new IOUtil();
                for (int i = 0; i < 10; i++) {
                    DownloadRequest request = new DownloadRequest("http://download.thinkbroadband.com/5MB.zip", Environment.getExternalStorageDirectory() + "/" + String.valueOf(i), i);
                    requests.add(request);
                    titles.add("Title " + i);
                    description.add("Description " + i);

                    tasks.add(new DownloadTask(request, emptyCallback, ioUtil));
                }

                
                new DownloadmanFactory(context, ioUtil).setDifferentNotificationForAll(100500, tasks, titles, description);
                new Downloading().enqueue(tasks);
            }
        });


```
### Setup notification ###

For single notification for all download task use this code

```java
new DownloadmanFactory(context, ioUtil).setOneNotificationForAll(100500, tasks, "Title", "Description");
```
Notification will looks like 

![one.png](https://bitbucket.org/repo/jbdnGr/images/4047392256-one.png)

If you want to show to user notification for each downloading task, use this code
```java
new DownloadmanFactory(context, ioUtil).setDifferentNotificationForAll(100500, tasks, titles, description);
```
Use will see something like this:

![many.png](https://bitbucket.org/repo/jbdnGr/images/3597104318-many.png)
### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact