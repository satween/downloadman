package com.satween.downloadman;

import android.os.Environment;
import android.test.InstrumentationTestCase;

import com.satween.commons.async.AsyncCallback;
import com.satween.commons.async.ProgressState;
import com.satween.commons.async.Response;
import com.satween.utils.IOUtil;

import java.io.FileNotFoundException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Clearview project<br />
 * http://www.clear--view.com<br />
 * Created by satween <br />
 */

public class DownloadTaskTest extends InstrumentationTestCase {

    private class SuccessResponseCallbackChecker implements AsyncCallback<String> {
        CountDownLatch signal = new CountDownLatch(1);
        String pathToSave;

        private SuccessResponseCallbackChecker(String pathToSave) {
            this.pathToSave = pathToSave;
        }

        @Override
        public void onSuccess(Response<String> response) {
            signal.countDown();
            IOUtil ioUtil = new IOUtil();
            if (!ioUtil.isFileExist(pathToSave)) {
                fail("File not exist");
            } else {
                try {
                    ioUtil.deleteFile(pathToSave);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }

        }

        @Override
        public void onError(Response<String> response) {
            fail("Error occurred");
        }

        @Override
        public void onProgressUpdate(ProgressState progress) {
            System.out.println("Progress updated " + progress.getProgress());
        }

        public CountDownLatch getSignal() {
            return signal;
        }
    }


    public void testValidDownloading() throws InterruptedException {

        final String pathToSave = Environment.getExternalStorageDirectory() + "/file_downloaded.jpeg";


        // Attention! This link may be unavailable in future. If test fails, try to replace with another url
        String urlToDownload = "http://m.c.lnkd.licdn.com/mpr/mpr/p/4/005/052/10a/0e7ce98.jpg";

        SuccessResponseCallbackChecker callback = new SuccessResponseCallbackChecker(pathToSave);

        DownloadTask task = Downloading.createDownloadTask(callback, urlToDownload, pathToSave);

        task.execute();

        if (!callback.getSignal().await(20, TimeUnit.SECONDS)) {
            fail("Downloading time is too long");
        }


    }
}
