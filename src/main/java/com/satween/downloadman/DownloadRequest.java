package com.satween.downloadman;

/**
 * Request for downloading
 * <p/>
 * https://github.com/satween/downloadman
 */

public class DownloadRequest {
    private String url;
    private String destinationPath;
    private long id;

    public DownloadRequest(String url, String destinationPath, long id) {
        this.url = url;
        this.destinationPath = destinationPath;
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public String getDestinationPath() {
        return destinationPath;
    }

    public long getId() {
        return id;
    }
}
