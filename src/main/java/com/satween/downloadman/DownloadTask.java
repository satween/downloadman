package com.satween.downloadman;

import android.os.AsyncTask;

import com.google.common.base.Optional;
import com.satween.commons.async.AsyncCallback;
import com.satween.commons.async.ProgressState;
import com.satween.commons.async.Response;
import com.satween.commons.async.ResponseStatus;
import com.satween.downloadman.notifications.DownloadingNotification;
import com.satween.utils.IOUtil;
import com.satween.utils.LogUtil;

import org.apache.http.conn.ConnectTimeoutException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * DownloadingTask responsible for downloading content
 * <p/>
 * https://github.com/satween/downloadman
 */

public class DownloadTask extends AsyncTask<String, ProgressState, Response<DownloadRequest>> {

    private static final int TIMEOUT = 3000;
    private LogUtil logger = LogUtil.get(this);


    private DownloadRequest request;
    private AsyncCallback<DownloadRequest> callback;
    private Optional<DownloadingNotification> notification;

    public DownloadTask(DownloadRequest request, AsyncCallback<DownloadRequest> callback, IOUtil ioUtil) {
        this.callback = callback;
        this.request = request;
        ioUtil.createFullPath(request.getDestinationPath());
    }

    public void setNotification(DownloadingNotification notification) {
        this.notification = Optional.fromNullable(notification);
    }

    @Override
    protected Response<DownloadRequest> doInBackground(String... strings) {
        return download();
    }


    private Response<DownloadRequest> download() {
        logger.logInfo("Starting downloading of " + request.getId());
        InputStream in = null;
        OutputStream out = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(request.getUrl());
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(TIMEOUT);
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return createResponse(ResponseStatus.getResponseByCode(connection.getResponseCode()));
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            ProgressState progress = new ProgressState(connection.getContentLength());

            // download the file
            in = connection.getInputStream();
            out = new FileOutputStream(request.getDestinationPath());


            byte data[] = new byte[4096];
            int count;
            while ((count = in.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    return createResponse(ResponseStatus.CANCELED);
                }

                publishProgress(progress.incrementProgress(count));
                out.write(data, 0, count);
            }
            out.flush();


            return createResponse(ResponseStatus.OK);

        } catch (ConnectTimeoutException e) {
            return createResponse(ResponseStatus.TIMED_OUT);
        } catch (IOException e) {
            return new Response<DownloadRequest>(ResponseStatus.EXCEPTION, request, e);
        } finally {

            if (in != null) {
                try {
                    in.close();
                } catch (IOException ignored) {

                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ignored) {

                }
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    private Response<DownloadRequest> createResponse(ResponseStatus status) {
        return new Response<DownloadRequest>(status, request);
    }


    @Override
    protected void onPostExecute(Response<DownloadRequest> response) {
        if (response.getError().isPresent()) {
            callback.onError(response);
            logger.logError("Failed downloading " + request.getId());
        } else {
            logger.logInfo("Success downloading " + request.getId() + " to " + request.getDestinationPath());
            callback.onSuccess(response);
        }
        if (notification.isPresent()) {
            notification.get().downloadingFinished();
        }
    }


    @Override
    protected void onProgressUpdate(ProgressState... values) {
        callback.onProgressUpdate(values[0]);
        if (notification.isPresent()) {
            notification.get().setProgress(values[0]).update();
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        callback.onPreExecute();
        if (notification.isPresent()) {
            notification.get().prepare();
        }
    }
}
