package com.satween.downloadman.notifications;

import android.app.NotificationManager;
import android.support.v4.app.NotificationCompat;

import com.satween.commons.async.ProgressState;

/**
 * <p/>
 * https://github.com/satween/downloadman
 */

public abstract class BaseDownloadingNotification implements DownloadingNotification {

    protected int id;
    protected NotificationManager manager;
    protected NotificationCompat.Builder builder;


    public BaseDownloadingNotification(int id, NotificationManager manager, NotificationCompat.Builder builder) {
        this.id = id;
        this.manager = manager;
        this.builder = builder;
    }

    @Override
    public void update() {
        manager.notify(id, builder.build());
    }

    @Override
    public void prepare() {
        builder.setProgress(0, 0, true);
        update();
    }


    protected void setProgress(int total, int current) {
        builder.setProgress(total, current, false);
    }

    public abstract BaseDownloadingNotification downloadingFinished();

    public abstract BaseDownloadingNotification setProgress(ProgressState progressState);


}
