package com.satween.downloadman.notifications;

import android.app.NotificationManager;
import android.support.v4.app.NotificationCompat;

import com.satween.commons.async.ProgressState;

/**
 * {@link com.satween.downloadman.notifications.MultipleTaskNotification} - this notification shows<br />
 * only one notification for all downloading content<br />
 * Use this type of notification if you want to download many files at the same time.
 * <p/>
 * https://github.com/satween/downloadman
 */

public class MultipleTaskNotification extends BaseDownloadingNotification {
    private int totalTasks;
    private int downloadedCount;

    public MultipleTaskNotification(int id, int totalTasksCount, NotificationManager manager, NotificationCompat.Builder builder) {
        super(id, manager, builder);
        this.totalTasks = totalTasksCount;
    }

    @Override
    public BaseDownloadingNotification setProgress(ProgressState progressState) {
        //do nothing
        return this;
    }

    @Override
    public BaseDownloadingNotification downloadingFinished() {
        setProgress(totalTasks, ++downloadedCount);
        update();

        if (downloadedCount == totalTasks) {
            manager.cancel(id);
        }
        return this;
    }
}
