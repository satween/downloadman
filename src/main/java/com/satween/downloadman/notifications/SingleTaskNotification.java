package com.satween.downloadman.notifications;

import android.app.NotificationManager;
import android.support.v4.app.NotificationCompat;

import com.satween.commons.async.ProgressState;

/**
 * {@link com.satween.downloadman.notifications.SingleTaskNotification} use this notification<br />
 * when you want to download less than five files at the same time. This type of notification
 * creates one notification with progress bar for each downloading file.
 * <p/>
 * https://github.com/satween/downloadman
 */

public class SingleTaskNotification extends BaseDownloadingNotification {
    public SingleTaskNotification(int id, NotificationManager manager, NotificationCompat.Builder builder) {
        super(id, manager, builder);
    }


    @Override
    public BaseDownloadingNotification downloadingFinished() {
        manager.cancel(id);
        return this;
    }

    @Override
    public BaseDownloadingNotification setProgress(ProgressState progressState) {
        setProgress(progressState.getTotal(), progressState.getCurrent());
        return this;
    }
}
