package com.satween.downloadman.notifications;

import com.satween.commons.async.ProgressState;

/**
 * <p/>
 * https://github.com/satween/downloadman
 */

public interface DownloadingNotification {

    /**
     * Updates view of progressbar notification
     */
    public void update();

    /**
     * This method execute before starting downloading.
     */
    public void prepare();

    /**
     * This method executes when downloading is finished. Use them to release any resources.
     *
     * @return should return current object
     */
    public BaseDownloadingNotification downloadingFinished();

    /**
     * This method executes while file is downloading. This method only set the new state of <br />
     * progress and not updates interface view. Use {@link #update()} method for re-render the ui.
     *
     * @param progressState new state of downloading progress.
     * @return should return current object
     */
    public BaseDownloadingNotification setProgress(ProgressState progressState);

}
