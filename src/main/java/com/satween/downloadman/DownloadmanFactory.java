package com.satween.downloadman;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.satween.commons.async.AsyncCallback;
import com.satween.downloadman.notifications.DownloadingNotification;
import com.satween.downloadman.notifications.MultipleTaskNotification;
import com.satween.downloadman.notifications.SingleTaskNotification;
import com.satween.utils.IOUtil;

import java.util.Arrays;
import java.util.List;

/**
 * <p/>
 * https://github.com/satween/downloadman
 */

public class DownloadmanFactory {

    private Context context;
    private IOUtil ioUtil;

    public DownloadmanFactory(Context context, IOUtil ioUtil) {
        this.context = context;
        this.ioUtil = ioUtil;
    }

    public DownloadTask createDownloadTask(AsyncCallback<DownloadRequest> callback, DownloadRequest request) {
        return new DownloadTask(request, callback, ioUtil);
    }

    public void setOneNotificationForAll(int id, List<DownloadTask> tasks, String title, String description) {

        DownloadingNotification downloadingNotification = createMultipleTasksNotification(id, tasks.size(), title, description);

        for (DownloadTask task : tasks) {
            task.setNotification(downloadingNotification);
        }
    }

    public void setDifferentNotificationForAll(int firstNotificationId, List<DownloadTask> tasks, List<String> titles, List<String> descriptions, List<Integer> resourceIds) {
        for (int i = 0; i < tasks.size(); i++) {
            tasks.get(i).setNotification(createSingleDownloadNotification(firstNotificationId++, titles.get(i), descriptions.get(i), resourceIds.get(i)));
        }
    }

    public void setDifferentNotificationForAll(int firstNotificationId, List<DownloadTask> tasks, List<String> titles, List<String> descriptions) {

        Integer[] resources = new Integer[tasks.size()];
        Arrays.fill(resources, R.drawable.downloading);
        List<Integer> defaultResources = Arrays.asList(resources);


        setDifferentNotificationForAll(firstNotificationId, tasks, titles, descriptions, defaultResources);
    }

    public DownloadingNotification createMultipleTasksNotification(int id, int total, String title, String description) {
        return new MultipleTaskNotification(id, total, getNotificationManager(), createBuilder(title, description, R.drawable.downloading));
    }

    public DownloadingNotification createSingleDownloadNotification(int id, String title, String description, int resourceId) {
        return new SingleTaskNotification(id, getNotificationManager(), createBuilder(title, description, resourceId));
    }

    private NotificationCompat.Builder createBuilder(String title, String description, int resourceId) {
        return new NotificationCompat.Builder(context).setContentTitle(title).setContentText(description).setAutoCancel(true).setSmallIcon(resourceId);
    }

    private NotificationManager getNotificationManager() {
        return (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }


}
