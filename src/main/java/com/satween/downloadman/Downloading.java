package com.satween.downloadman;

import java.util.List;

/**
 * <p/>
 * https://github.com/satween/downloadman
 */

public class Downloading {

    public void enqueue(List<DownloadTask> tasks) {
        for (DownloadTask task : tasks) {
            task.execute();
        }
    }
}
